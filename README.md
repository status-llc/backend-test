# About VMoll.Backend

VMoll.Backend is ASP.NET MVC / WebAPI application.

## Class diagram (DB implementation)

![Class diagram](https://bitbucket.org/status-llc/vmoll-backend/raw/master/VMoll.Backend/Diagrams/vmoll-classes-1.svg)